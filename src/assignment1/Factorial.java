package assignment1;

public class Factorial {

//	2. Write a program to find the factorial of given number.
	public static void main(String[] args) {
		
		int number =5, factorial=1;
		
		for(int i =1; i<=number; i++) {
			factorial = factorial*i;
		}
		System.out.println("Factorial of "+ number+ " is : "+ factorial);
		
	}

}
