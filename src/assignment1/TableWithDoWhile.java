package assignment1;

public class TableWithDoWhile {

	public static void main(String[] args) {
		int n=10, i=1;
		
		do {
			System.out.println(n + "X" + i + " = "+ (n*i));
			i++;
		} while (i<=10);

	}

}
