package assignment1;

public class InstanceAndStaticVariable {

	//Assignment: define 2 instance variables and 2 static variables, call those variables inside instance method and static method.
	// call those instance & static methods in main method
	
//	instance variable
	int a = 10, a1;
//	static variable
	static int b = 25, b1;
	
//	instance method
	
	public void instanceMethod1() {
		
		System.out.println("instance method instance vraiable" + a);
		System.out.println("instance method static vraiable" + b);
		System.out.println("instance method static vraiable- recomanded" + InstanceAndStaticVariable.b);
		
	}
	
//	static method
	public static void staticMethod() {
		
		InstanceAndStaticVariable istv = new InstanceAndStaticVariable();
		System.out.println("static method instance vraiable" + istv.a);
		System.out.println("staic method static vraiable" + b);
		
	}
	
	public static void main(String[] args) {
		
		staticMethod();
		InstanceAndStaticVariable istv= new InstanceAndStaticVariable();
		istv.instanceMethod1();
		System.out.println("main method instance vraiable" + istv.a);
		System.out.println("main method static vraiable" + b);
		
		
	}

}
