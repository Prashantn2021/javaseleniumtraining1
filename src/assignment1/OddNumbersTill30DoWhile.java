package assignment1;

public class OddNumbersTill30DoWhile {

	public static void main(String[] args) {
		
		int i=1;
		System.out.println("Odd Numbers: ");
		
		do {
			if(i%2!=0) {
				System.out.println(i);
				
			}
			i++;
		}while (i<=30);
		
	}

}
