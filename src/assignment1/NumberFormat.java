package assignment1;

public class NumberFormat {

	public static void main(String[] args) {
		
		/*
		 * format
		 * 1
		 * 1 2
		 * 1 2 3 
		 * 1 2 3 4 
		 * 1 2 3 4 5
		 */
//		int n=5;
//		for(int i=1; i<=n; i++) {
//			for (int j=1; j<=i; j++) {
//				System.out.print(j);
//			}
//			System.out.println();
//		}
//		
		/*
		 * format
		 * 1 2 3 4 5
		 * 1 2 3 4
		 * 1 2 3  
		 * 1 2 
		 * 1
		 */
	
		int n=5;
		for(int i=n; i>=1; i--) {
			for (int j=1; j<=i; j++) {
				System.out.print(j);
			}
			System.out.println();
		}
		
		
	}

}
