package assignment7_ArrayList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class ArrayListDataRetrival5 {

	public static void main(String[] args) {
		
//		5. Write a program to add 6 string objects into ArrayList and display all the data in console using for, for each, Iterator and ListIterator.

		ArrayList<String> al = new ArrayList<String>();
		
		al.add("Ram");
		al.add("Naveen");
		al.add("Santosh");
		al.add("Praveen");
		al.add("Tom");
		al.add("Radha");
		
		System.out.println("*********Using For Loop**************");
		
		for(int i=1; i<al.size(); i++) {
			
			System.out.println(al.get(i));
			
		}
		
		System.out.println("*********Using For Each Loop**************");
		
		for(String name: al) {
			System.out.println(name);
		}
		
		System.out.println("*********Using Iterator**************");
		
		Iterator<String> itr = al.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
			
		}
		System.out.println("*********Using IteratorList**************");
		ListIterator<String> litr =al.listIterator();
		
		while(litr.hasNext()) {
		
			System.out.println(litr.next());
		}
		
		
	}

}
