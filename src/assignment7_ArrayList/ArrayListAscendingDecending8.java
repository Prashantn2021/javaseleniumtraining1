package assignment7_ArrayList;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayListAscendingDecending8 {

	public static void main(String[] args) {
//		8. Write a program for the following scenarios:
//			a. Add 7 string objects into one ArrayList
//			b. Display all the objects in ascending order
//			c. Display all the objects in descending order using 2 possible ways.
		
		
		ArrayList<String> al = new ArrayList<String>();
		al.add("Test2");
		al.add("Test7");
		al.add("Test3");
		al.add("Test1");
		al.add("Test5");
		al.add("Test6");
		al.add("Test7");
		al.add("Test4");
		
		System.out.println("Orginal ArrayList: "+al);
		
		Collections.sort(al);
		System.out.println("Sorted with Ascending Order"+al);
		
		Collections.reverse(al);
		System.out.println("Sorted with Decending Order"+al);
	}

}
