package assignment7_ArrayList;

import java.util.ArrayList;

public class AddStringObjectsArrayList1 {
	
//	List interface:
//		1. Write a program for the following scenarios:
//		a. Add 5 string objects into one ArrayList

	public static void main(String[] args) {

		
		ArrayList<String> al1 = new ArrayList<String>();
		
//		a. Add 5 string objects into one ArrayList
		
		al1.add("India");
		al1.add("Japan");
		al1.add("Australia");
		al1.add("UK");
		al1.add("USA");
		
		System.out.println("5 County added Original List: "+al1);
		
//		b. Insert one string object at 2nd position //add(obj)	add(1,"")
		
		al1.add(1, "SriLanka");
		
		System.out.println("added Srilanka at 2nd poistion: "+al1);
		
//		c. Delete the value at 3rd index //remove(3)
		
		al1.remove(2);
		System.out.println("removed country from 3rd index: "+al1);
		
		
//		d. Update the value at 3rd position with 1st index value //get(1)
		
		al1.set(2, al1.get(0));
		System.out.println("Updated the value at 3rd position with 1st index value: "+ al1);
		
//		e. Display the value of 4th position in console
		
		System.out.println("value of 4th position: "+al1.get(3));
		
		
//		f. Display the count of objects available in ArrayList.
		
		System.out.println("count of objects available in ArrayList: "+al1.size());
		
		

	}

}
