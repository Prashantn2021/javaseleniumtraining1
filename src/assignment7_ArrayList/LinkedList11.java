package assignment7_ArrayList;

import java.util.LinkedList;

public class LinkedList11 {

	public static void main(String[] args) {
//		11. Write a program for the following scenarios:
//			a. Add 4 string objects into one LinkedList
//			b. Add one object at first position and last position
//			c. Delete the object at 3rd position
//			d. Update the value at 4th position with 2nd index value
		
		
		LinkedList<String> ll= new LinkedList<String>();
		
		ll.add("Kashmir");
		ll.add("UP");
		ll.add("Andhra");
		ll.add("karnataka");
		
		System.out.println(ll);
		
		ll.add(0, "MP");
		ll.addFirst("Maharashtra");
		System.out.println(ll);
		
		ll.addLast("tamilnadu");
		System.out.println(ll);
		
		ll.remove(2);
		System.out.println(ll);
		
		ll.set(3, ll.get(1));
		System.out.println(ll);
		
	}

}
