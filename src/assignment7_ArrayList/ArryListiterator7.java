package assignment7_ArrayList;

import java.util.ArrayList;
import java.util.ListIterator;

public class ArryListiterator7 {

	public static void main(String[] args) {
		// 7. Write a program for the following scenarios:
//		a. Add 4 string objects (ex: element1, element10, element3, element4) into one ArrayList
//**		b. Add one more string object(ex: element5) in above ArrayList using ListIterator
//	**	c. Update the value "element10" with "element2" using ListIterator
//	**	d. Delete the value at last position using ListIterator
		
		ArrayList<String> al = new ArrayList<String>();
		al.add("element1");
		al.add("element10");
		al.add("element3");
		al.add("element4");
	
		ListIterator<String> litr = al.listIterator();
		while(litr.hasNext()) {
			litr.add("elememt5");
			System.out.println(litr.next());
			
		}
		System.out.println(al);
		
//		c. Update the value "element10" with "element2" using ListIterator
		
		al.set(3, "element2");
		System.out.println(al);
		
		
//		**	d. Delete the value at last position using ListIterator
		
		System.out.println(al.size());
		System.out.println(al.size()-1);
		int a =al.size()-1;
		al.remove(a);
		System.out.println(al);
		
		
	}

}
