package assignment7_ArrayList;

import java.util.ArrayList;
import java.util.LinkedList;

public class ArrayListLinkedList12 {

	public static void main(String[] args) {
//		12. Write a program to add all String objects from one ArrayList to other LinkedList without using addAll() and then print the objects of LinkedList in console.

		ArrayList<String> al = new ArrayList();
		al.add("Banaglore");
		al.add("Hyderabad");
		al.add("Chennai");
		al.add("Mumbai");
		
		LinkedList<String> ll= new LinkedList<String>();
		ll.add("Delhi");
		for(int i=0;i<al.size();i++) {
			ll.add(al.get(i));
		}
		ll.add("kolkatta");
		System.out.println(ll);

	}

}
