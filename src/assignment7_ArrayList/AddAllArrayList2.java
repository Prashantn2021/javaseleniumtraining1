package assignment7_ArrayList;

import java.util.ArrayList;

public class AddAllArrayList2 {

	public static void main(String[] args) {
		
//		2. Write a program to add objects of two ArrayLists into another ArrayList.
		
		ArrayList<String> al1 = new ArrayList<String>();
		
		al1.add("India");
		al1.add("USA");
		al1.add("UK");
		al1.add("Australia");
		
		System.out.println("al1 elements: "+al1);
		ArrayList<String> al2 = new ArrayList<String>();
		
		al2.add("Bangalore");
		al2.add("Mumbai");
		al2.add("Delhi");
		al2.add("Hyderabad");
		
		al2.addAll(al1);
		System.out.println("After adding al1 element s in al2: "+al2);
		
//		3. Write a program to store the list of objects from 1st index to 4th index from one ArrayList to another ArrayList

		al2.addAll(3, al2);
		System.out.println("store the list of objects from 1st index to 4th index from one ArrayList to another ArrayList"+al2);
		
//***	4. Write a program for the following scenarios:
//			a. Delete all the objects from first ArrayList that are contained in second ArrayList

		al2.retainAll(al1);
		
		System.out.println("Delete all the objects from first ArrayList that are contained in second ArrayList"+al2);
		
		
//			b. Delete all the objects from first ArrayList that are NOT contained in second ArrayList
		
		al2.removeAll(al1);
		System.out.println(al2);

		

	}

}
