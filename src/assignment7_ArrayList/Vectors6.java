package assignment7_ArrayList;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

public class Vectors6 {

	public static void main(String[] args) {
//		6. Write a program to add 3 string objects into Vector and display all the data in console using all possible ways.

		Vector<String> vt= new Vector<String>();
		
		vt.add("Red");
		vt.add("Blue");
		vt.add("Green");
		vt.add("Yellow");
		
		System.out.println(vt);
		
		System.out.println("***** using enumaration *****");
		Enumeration<String> en= vt.elements();
		while(en.hasMoreElements()) {
			System.out.println(en.nextElement());
		}
		System.out.println("***** using Iterator *****");
		
		Iterator<String> itr = vt.iterator();
				
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
		
		
		System.out.println("***** using ListIterator *****");
		
		ListIterator<String> ltr = vt.listIterator();
		while(ltr.hasNext()) {
			System.out.println(ltr.next());
		}
		
		
		System.out.println("***** using for each *****");
		
		for(String str: vt) {
			System.out.println(str);
		}
		
		
	}

}
