package assignment7_ArrayList;

import java.util.ArrayList;
import java.util.Arrays;

public class ArayToArrayList9 {

	public static void main(String[] args) {
//		9. Write a program for the following scenarios:
//			a. Create one string array with 3 values and convert this array as ArrayList and then print the values from ArrayList individually.
//			b. Add 4 string objects into one ArrayList and Convert this ArrayList as Array and then print the values from Array individually.

//		a. Create one string array with 3 values 
		String[] names = {"Rahul", "Sachin", "Saurav"};
		
		for (String str:names) {
		System.out.println("Arrays Values: "+str);
		}
		
		
//		and convert this array as ArrayList and then print the values from ArrayList individually.
		ArrayList<String> al = new ArrayList<String>(Arrays.asList(names));
		
		System.out.println("ArrayList Values: "+al);
		
//		b. Add 4 string objects into one ArrayList 
		
		al.add("Virat");
		al.add("Anil");
		al.add("Vinod");
		al.add("Prasad");
		
		
		System.out.println("arralyList after adding 4 values: "+al);
//		and Convert this ArrayList as Array and then print the values from Array individually.

		String[] arr = new String[al.size()];
		al.toArray(arr);
		
		for (String str:arr) {
			System.out.println("Arrays Values: "+str);
			}
		
		
		
	}

}
